import React, { Component } from 'react';

export default class FetchApiExample extends Component {

    constructor(props) {
        super(props);

        this.baseUrl = "https://picsum.photos";
        this.apiUrl = "/v2/list?limit=8";

        this.state = {
            images: null,
        }
    }

    componentDidMount() {
        fetch(this.baseUrl + this.apiUrl )
            .then(response => response.json()
                .then(images => {
                    this.setState({images});
                }));
    }


    render(){
        if (!this.state.images) return <p>Loading...</p>;

        const images = this.state.images;

        return (
            <section className={"projectWrapper"}>
                {  images.map( ({author, url, id,}) =>
                    <div key={id} className={"projectCard"}>
                        <a href={url}>
                            <img src={this.baseUrl + "/id/" + id + "/250/300.jpg"} alt={"Image de " + author} />
                            <p>Auteur : {author}</p>
                        </a>
                    </div>
                )}
            </section>
        );
    }

}