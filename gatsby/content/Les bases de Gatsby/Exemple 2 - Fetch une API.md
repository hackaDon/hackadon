---
title: 'Exemple Fetch API'
metaTitle: "Exemple Fetch API"
metaDescription: "Comment utiliser javascript pour fetch l'API de la PFE et l'incorporer dans Gatsby"
---
import FetchApiExample from '../../src/components/FetchApiExample'

Des images au hasard :

<FetchApiExample />