---
title: 'Présentation Gatsby'
metaTitle: "Présentation Gatsby"
metaDescription: 'Introduction à Gatsby'
---

## Introduction

Le site sur lequel vous naviguez, ainsi que celui inclu dans le dossier Gatsby de votre framagit :

* ont été construits avec le générateur de site statique [GatsbyJS](https://www.gatsbyjs.org/),
* sont basés sur le [starter](https://www.gatsbyjs.org/tutorial/part-one/#using-gatsby-starters) répondant au doux nom de [gatsby-gitbook-starter](https://github.com/hasura/gatsby-gitbook-starter),
* sont hébergés sur frama.io via le système des [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). 

Si tout ça vous intéresse, je vous encourage à aller jeter un coup d'oeil aux différents liens ! Mais concrètement, qu'est-ce que ça veut dire dans notre cas ?

1. Que les pages des sites sont générés à partir de fichiers Markdown
2. Que ces fichiers Markdown sont en fait des MDX (Markdown + JSX) et qu'il est donc possible d'y inclure des composants React
3. Qu'il suffit de faire un `git push` pour mettre à jour le site.

## Par où commencer ?

Pour commencer à éditer le site, il vous faut en premier lieu cloner votre repository git :

```bash
git clone [votre_repository_framagit]
```

Ensuite, pour faire fonctionner le site en local, vous aurez besoin de docker-compose. Si besoin, vous trouverez les instructions d'installation sur le [site officiel](https://docs.docker.com/compose/install/).

Ensuite, il suffit de lancer la machine docker 

```bash
cd [dossier_local_du_repository]
docker-compose up
```

Attendez quelques instant et le site devrait être accessible à http://localhost:1111

À partir de maintenant, toutes les modifications que vous ferez seront répercutés directement et visible sur le localhost. Le dossier `gatsby/content/` contient les pages au format MDX, le dossier `gatsby/src/` contient les composants Gatsby/React nécessaires à leur génération. Selon vos besoins, vous pourriez avoir à modifier ces différents fichiers (voir d'autres !).

Si vous voulez avoir votre site en ligne, il vous faudra mettre en place une gitlab page, commit et push le tout !

Dans les pages suivantes, on vous donne les bases du Markdown et du MDX. Bonne lecture !

