---
title: 'Markdown : les bases'
metaTitle: "Markdown : les bases"
metaDescription: 'Introduction au Markdown'
---

Le Markdown est un langage qui se veut très simple et rapide à écrire. Sa structure fait qu'il est très facilement convertible en HTML et autres formats ([<3](https://pandoc.org/)).

Sur ce site, toutes les pages sont écrites en Markdown (.md) et les fichiers doivent avoir un en-tête suivant ce format :

```markdown
---
title: 'Markdown : les bases'
metaTitle: "Markdown : les bases"
metaDescription: 'Introduction au Markdown'
---
```

Il existe de nombreuses ressources sur le web expliquant les bases de la syntaxe Markdown donc on ne va pas réinventer la roue ici, on vous laisse les consulter. Celle-ci est tout de même un bon départ : https://rmarkdown.rstudio.com/authoring_basics.html

De manière générale, n'hésitez pas à regarder comment les autres pages ont été construites et à vous en inspirer.


