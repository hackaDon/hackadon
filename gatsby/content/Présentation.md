---
title: "Présentation"
metaTitle: "Présentation HackaDON"
---

Vous pouvez retrouver sur cette page la présentation du HackaDON qui vous a été faite lors de la soirée d'ouverture.

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTwVO05AdyehreY3Gk55wPTqLq9zOqOATnq7T_ef3G-KNA8IP3quL6Kf_YFuFLLfoqLWAl8NeArhDkO/embed?start=false&loop=false&delayms=5000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>