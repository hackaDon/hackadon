---
title: 'Hackadon 2020'
metaTitle: "HackaDon 2020 !"
metaDescription: 'description du HackaDon 2020'
---

Bienvenue sur le site du HackaDon 2020 !

Vous trouverez sur ce site :
* les [informations sur les premiers pas](./Premiers%20Pas),
* les technos disponibles et mises à disposition, 
* toutes les informations utiles avant, pendant et après ce superbe HackaDON 2020 !

**N'hésitez pas à y revenir régulièrement !**

## Communication sur les réseaux sociaux

Utilisez le hashtag suivant pour vos communications : [```#HACKADON```](https://twitter.com/hashtag/HackaDon)

